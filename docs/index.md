---
author: Ronan
title: 🏡 Accueil
---

A vous de personnaliser cet accueil

!!! info "Adapter ce site modèle"

    Le tutoriel est ici : [Tutoriel de site avec python](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/){:target="_blank" }
    
    Si vous voulez conserver certaines pages de ce modèles sans qu'elles ne soient visibles dans le menu, il suffit de les enlever du fichier .pages   
    Vous les retrouverez facilement en utilisant la barre de recherche en haut à droite
    

😊  Bienvenue !

On veut une fonction `Euclide` qui renvoie le PGCD de deux entiers par la méthode d'Euclide.


??? note "Rappel"

  On divise le plus grand des deux nombres par le plus petit et on recommence avec le diviseur et le reste jusqu'à ce que le reste soit nul. Dans une DE $a=bq+r$ le quotient de $a$ par $b$ est donné par `#!py a//b` et le reste par `#!py a%b`. 

??? example "Exemples"

    ```pycon title=""
    >>> Euclide(22,12)
    2
    >>> Euclide(22,7)
    1
    ```

=== "Version vide"
    {{ IDE('euclide_vide')}}
=== "Version à compléter"
    {{ IDE('euclide_trous')}}






